# Corrupted RSS / Atom Feed Collection

## Caution
These scripts are designed for Testing Purpose. **Do not attack somewhere
platforms, or your behavior might be reason for arrest**

## What this?
These are RSS / Atom feed correction to check whether the app has XSS or XXE
vulnerabilities.

## Why I made this?
In Japan, many people love programing, but as far as I checked, there's also
many programmers who doesn't care about their security. For example, a certain
job-hunting platform has many XSS vulnerability. Fortunately or Unfortunately,
I found them and I reported. However, there's not only XSS method that crackers
have. For example, RSS / Atom feed is written in XML, which support
**XML eXternal Entity**.

## Detail?
Check [xxe.rss](xxe.rss).

